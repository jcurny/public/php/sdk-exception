<?php
namespace Jcurny\Sdk\Exception\Business;

class LoginTimeoutException extends AbstractException
{
    protected $message = 'Login timeout exception';
    protected $code = 440;
}
