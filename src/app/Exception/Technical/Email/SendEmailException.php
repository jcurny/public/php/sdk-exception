<?php
namespace Jcurny\Sdk\Exception\Technical\Email;

class SendEmailException extends \Jcurny\Sdk\Exception\Technical\InternalServerErrorException
{
    protected $message = 'Send email exception';
}
