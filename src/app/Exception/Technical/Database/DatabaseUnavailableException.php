<?php
namespace Jcurny\Sdk\Exception\Technical\Database;

class DatabaseUnavailableException extends \Jcurny\Sdk\Exception\Technical\ServiceUnavailableException
{
    protected $message = 'Database unavailable exception';
}
