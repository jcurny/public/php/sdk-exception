<?php
namespace Jcurny\Sdk\Exception\Business\User;

class UserNotFoundException extends \Jcurny\Sdk\Exception\Business\NotFoundException
{
    protected $message = 'User not found exception';
}
