<?php
namespace Jcurny\Sdk\Exception\Business\User;

class UserLockedException extends \Jcurny\Sdk\Exception\Business\LockedException
{
    protected $message = 'User locked exception';
}
