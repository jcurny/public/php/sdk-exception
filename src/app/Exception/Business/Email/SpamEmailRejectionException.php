<?php
namespace Jcurny\Sdk\Exception\Business\Email;

class SpamEmailRejectionException extends \Jcurny\Sdk\Exception\Business\ForbiddenException
{
    protected $message = 'Spam email rejection exception';
}
