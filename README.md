# jcurny/sdk-exception

## Abstract

- AbstractException
- Business\AbstractException
- Technical\AbstractException

## Generic

### Business

- Business\BadRequestException
- Business\ConflictException
- Business\ForbiddenException
- Business\GoneException
- Business\LockedException
- Business\NotFoundException
- Business\PreconditionRequiredException
- Business\UnauthorizedException

### Technical

- Technical\InternalServerErrorException
- Technical\NotImplementedException
- Technical\ServiceUnavailableException
- Technical\UnknownErrorException

## Database

- Technical\Database\DatabaseUnavailableException
- Technical\Database\DatabaseUnknowErrorException

## Entity

- Business\Entity\EntityAlreadyExistsException
- Business\Entity\EntityNotFoundException

## User

- Business\User\UserAlreadyExistsException
- Business\User\UserBannedException
- Business\User\UserDeletedException
- Business\User\UserKickedException
- Business\User\UserLockedException
- Business\User\UserNotConfirmedException
- Business\User\UserNotFoundException
