<?php
namespace Jcurny\Sdk\Exception\Business;

class BadRequestException extends AbstractException
{
    protected $message = 'Bad request exception';
}
