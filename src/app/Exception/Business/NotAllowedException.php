<?php
namespace Jcurny\Sdk\Exception\Business;

class NotAllowedException extends AbstractException
{
    protected $message = 'Not allowed exception';
    protected $code = 405;
}
