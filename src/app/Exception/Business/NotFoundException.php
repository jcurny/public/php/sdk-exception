<?php
namespace Jcurny\Sdk\Exception\Business;

class NotFoundException extends AbstractException
{
    protected $message = 'Not found exception';
    protected $code = 404;
}
