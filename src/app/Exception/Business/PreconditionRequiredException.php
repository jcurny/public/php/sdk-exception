<?php
namespace Jcurny\Sdk\Exception\Business;

class PreconditionRequiredException extends AbstractException
{
    protected $message = 'Precondition required exception';
    protected $code = 428;
}
