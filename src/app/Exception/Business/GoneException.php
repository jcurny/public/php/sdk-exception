<?php
namespace Jcurny\Sdk\Exception\Business;

class GoneException extends AbstractException
{
    protected $message = 'Gone exception';
    protected $code = 410;
}
