<?php
namespace Jcurny\Sdk\Exception\Technical;

class InternalServerErrorException extends AbstractException
{
    protected $message = 'Internal server error exception';
    protected $code = 500;
}
