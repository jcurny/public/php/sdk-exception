<?php
namespace Jcurny\Sdk\Exception\Business\Entity;

class EntityAlreadyExistsException extends \Jcurny\Sdk\Exception\Business\ConflictException
{
    protected $message = 'Entity already exists exception';
}
