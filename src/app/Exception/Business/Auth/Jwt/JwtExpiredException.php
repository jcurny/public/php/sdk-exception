<?php
namespace Jcurny\Sdk\Exception\Business\Auth\Jwt;

class JwtExpiredException extends \Jcurny\Sdk\Exception\Business\LoginTimeoutException
{
    protected $message = 'Jwt expired exception';
}
