<?php
namespace Jcurny\Sdk\Exception\Technical;

class ServiceUnavailableException extends AbstractException
{
    protected $message = 'Service unavailable exception';
    protected $code = 503;
}
