<?php
namespace Jcurny\Sdk\Exception\Business\Email;

class HardBounceEmailRejectionException extends \Jcurny\Sdk\Exception\Business\ForbiddenException
{
    protected $message = 'Hard bounce email rejection exception';
}
