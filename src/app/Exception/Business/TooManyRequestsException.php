<?php
namespace Jcurny\Sdk\Exception\Business;

class TooManyRequestsException extends AbstractException
{
    protected $message = 'Too many requests';
    protected $code = 429;
}
