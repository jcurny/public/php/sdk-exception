<?php
namespace Jcurny\Sdk\Exception\Business\Media;

class MediaTooSmallException extends \Jcurny\Sdk\Exception\Business\BadRequestException
{
    protected $message = 'Media too small exception';
}
