<?php
namespace Jcurny\Sdk\Exception\Business\Api\Route;

class RouteForbiddenException extends \Jcurny\Sdk\Exception\Business\ForbiddenException
{
    protected $message = 'Route forbidden exception';
}
