#!/bin/bash -ex

ME=`basename "$0"`

if [[ ! -f .ci/hook/$ME ]]; then
  echo "Launch this from the root folder"
  exit 1
fi

find . -type f -iregex '.*\.\(php\|phtml\)$' -not -path "./vendor/*" -exec php -l {} \; | (! grep -v "No syntax errors detected" )
