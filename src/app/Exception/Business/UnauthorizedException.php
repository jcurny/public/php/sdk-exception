<?php
namespace Jcurny\Sdk\Exception\Business;

class UnauthorizedException extends AbstractException
{
    protected $message = 'Unauthorized exception';
    protected $code = 401;
}
