<?php
namespace Jcurny\Sdk\Exception\Business\Auth\Jwt;

class JwtForbiddenException extends \Jcurny\Sdk\Exception\Business\ForbiddenException
{
    protected $message = 'Jwt forbidden exception';
}
