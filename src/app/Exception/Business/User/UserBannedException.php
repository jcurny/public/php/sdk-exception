<?php
namespace Jcurny\Sdk\Exception\Business\User;

class UserBannedException extends \Jcurny\Sdk\Exception\Business\GoneException
{
    protected $message = 'User banned exception';
}
