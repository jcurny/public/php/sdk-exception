<?php
namespace Jcurny\Sdk\Exception\Business\Media;

class MediaBadFormatException extends \Jcurny\Sdk\Exception\Business\BadRequestException
{
    protected $message = 'Media bad format exception';
}
