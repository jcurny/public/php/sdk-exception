<?php
namespace Jcurny\Sdk\Exception\Business\User;

class UserAlreadyExistsException extends \Jcurny\Sdk\Exception\Business\ConflictException
{
    protected $message = 'User already exists exception';
}
