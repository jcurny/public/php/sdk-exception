<?php
namespace Jcurny\Sdk\Exception\Technical\Runtime\ELException;

class MethodNotFoundException extends \Jcurny\Sdk\Exception\Technical\Runtime\AbstractRuntimeException
{
    protected $message = 'Method not found exception';
}
