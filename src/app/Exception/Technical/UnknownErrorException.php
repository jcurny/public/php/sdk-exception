<?php
namespace Jcurny\Sdk\Exception\Technical;

class UnknownErrorException extends AbstractException
{
    protected $message = 'Unknown error exception';
    protected $code = 520;
}
