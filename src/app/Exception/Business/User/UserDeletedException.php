<?php
namespace Jcurny\Sdk\Exception\Business\User;

class UserDeletedException extends \Jcurny\Sdk\Exception\Business\GoneException
{
    protected $message = 'User deleted exception';
}
