<?php
namespace Jcurny\Sdk\Exception\Technical;

class NotImplementedException extends AbstractException
{
    protected $message = 'Not implemented exception';
    protected $code = 501;
}
