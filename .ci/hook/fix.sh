#!/bin/bash -ex

ME=`basename "$0"`

if [[ ! -f .ci/hook/$ME ]]; then
  echo "Launch this from the root folder"
  exit 1
fi

cat << 'EOF' > .php_cs
<?php
$finder = PhpCsFixer\Finder::create()
  ->in(__DIR__)
  ->exclude([
    '.git',
    'vendor',
  ])
;

return PhpCsFixer\Config::create()
  ->setFinder($finder)
  ->setRules([
    '@PSR1' => true,
    '@PSR2' => true,
    '@PhpCsFixer' => true,

    // Risky rules
    'native_function_invocation' => true,
    'psr4' => true,

    // Rules
    'general_phpdoc_annotation_remove' => [
      'annotations' => ['author', 'package']
    ],
    'header_comment' => [
      'header' => '',
    ],
      'no_blank_lines_before_namespace' => true,
      'single_blank_line_before_namespace' => false
    ]
  )
;
EOF

jq '.' composer.json | sponge composer.json

php-cs-fixer --allow-risky=yes --verbose fix || true
rm -f .php_cs.cache
