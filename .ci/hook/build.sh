#!/bin/bash -ex

ME=`basename "$0"`

if [[ ! -f .ci/hook/$ME ]]; then
  echo "Launch this from the root folder"
  exit 1
fi

display_usage() {
  ME=`basename "$0"`
  echo "Usage: $ME <branch>"
}

if [ "$#" -ne 1 ]; then
  display_usage
  exit 1
fi

if [[ "$1" == "master" ]]; then
  echo "Not allowed on master"
  exit 1
else
  echo "Composer..."
  composer update --no-dev --no-progress --prefer-dist --prefer-stable
fi
