<?php
namespace Jcurny\Sdk\Exception\Business;

class TooEarlyException extends AbstractException
{
    protected $message = 'Too early exception';
    protected $code = 425;
}
