<?php
namespace Jcurny\Sdk\Exception\Technical\Runtime;

abstract class AbstractRuntimeException extends \Jcurny\Sdk\Exception\Technical\InternalServerErrorException
{
    protected $message = 'Runtime exception';
}
