<?php
namespace Jcurny\Sdk\Exception\Business\Auth\Jwt;

class JwtUnauthorizedException extends \Jcurny\Sdk\Exception\Business\UnauthorizedException
{
    protected $message = 'Jwt unauthorized exception';
}
