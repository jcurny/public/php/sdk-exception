<?php
namespace Jcurny\Sdk\Exception\Technical\Database;

class DatabaseUnknownErrorException extends \Jcurny\Sdk\Exception\Technical\UnknownErrorException
{
    protected $message = 'Database unknown error exception';
}
