<?php
namespace Jcurny\Sdk\Exception\Business\Email;

class SoftBounceEmailRejectionException extends \Jcurny\Sdk\Exception\Business\ForbiddenException
{
    protected $message = 'Soft bounce email rejection exception';
}
