<?php
namespace Jcurny\Sdk\Exception\Business\Auth\Jwt;

class JwtTooEarlyException extends \Jcurny\Sdk\Exception\Business\TooEarlyException
{
    protected $message = 'Jwt too early exception';
}
