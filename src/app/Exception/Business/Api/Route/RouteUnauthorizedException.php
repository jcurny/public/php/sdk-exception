<?php
namespace Jcurny\Sdk\Exception\Business\Api\Route;

class RouteUnauthorizedException extends \Jcurny\Sdk\Exception\Business\UnauthorizedException
{
    protected $message = 'Route unauthorized exception';
}
