<?php
namespace Jcurny\Sdk\Exception\Business\Media;

class MediaNotFoundException extends \Jcurny\Sdk\Exception\Business\NotFoundException
{
    protected $message = 'Media not found exception';
}
