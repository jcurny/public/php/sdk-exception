<?php
namespace Jcurny\Sdk\Exception\Business;

class LockedException extends AbstractException
{
    protected $message = 'Locked exception';
    protected $code = 423;
}
