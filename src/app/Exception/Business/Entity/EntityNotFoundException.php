<?php
namespace Jcurny\Sdk\Exception\Business\Entity;

class EntityNotFoundException extends \Jcurny\Sdk\Exception\Business\NotFoundException
{
    protected $message = 'Entity not found exception';
}
