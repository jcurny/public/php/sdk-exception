<?php
namespace Jcurny\Sdk\Exception\Business\Api\Route;

class RouteNotFoundException extends \Jcurny\Sdk\Exception\Business\NotFoundException
{
    protected $message = 'Route not found exception';
}
