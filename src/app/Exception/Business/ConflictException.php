<?php
namespace Jcurny\Sdk\Exception\Business;

class ConflictException extends AbstractException
{
    protected $message = 'Conflict exception';
    protected $code = 409;
}
