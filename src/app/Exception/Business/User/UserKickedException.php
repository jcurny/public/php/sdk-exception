<?php
namespace Jcurny\Sdk\Exception\Business\User;

class UserKickedException extends \Jcurny\Sdk\Exception\Business\GoneException
{
    protected $message = 'User kicked exception';
}
