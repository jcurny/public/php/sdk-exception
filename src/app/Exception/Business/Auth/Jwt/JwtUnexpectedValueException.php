<?php
namespace Jcurny\Sdk\Exception\Business\Auth\Jwt;

class JwtUnexpectedValueException extends \Jcurny\Sdk\Exception\Business\BadRequestException
{
    protected $message = 'Jwt unexpected value exception';
}
