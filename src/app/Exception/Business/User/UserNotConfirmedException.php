<?php
namespace Jcurny\Sdk\Exception\Business\User;

class UserNotConfirmedException extends \Jcurny\Sdk\Exception\Business\PreconditionRequiredException
{
    protected $message = 'User not confirmed exception';
}
