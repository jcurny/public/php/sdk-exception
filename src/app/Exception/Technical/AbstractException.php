<?php
namespace Jcurny\Sdk\Exception\Technical;

abstract class AbstractException extends \Jcurny\Sdk\Exception\AbstractException
{
    protected $message = 'Technical exception';
    protected $code = 500;
}
