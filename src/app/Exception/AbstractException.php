<?php
namespace Jcurny\Sdk\Exception;

use Throwable;

abstract class AbstractException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Exception';

    /**
     * @var int
     */
    protected $code = 0;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Exception constructor.
     *
     * @param string    $message  [optional] The Exception message to throw
     * @param int       $code     [optional] The Exception code
     * @param throwable $previous [optional] The previous throwable used for the exception chaining
     * @param array     $data     [optional] The Exception data
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null, array $data = [])
    {
        $message = !empty($message) ? $message : $this->message;
        $code = !empty($code) ? $code : $this->code;
        parent::__construct($message, $code, $previous);
        $this->data = $data;
    }

    /**
     * @return array
     */
    final public function getData()
    {
        return $this->data;
    }
}
