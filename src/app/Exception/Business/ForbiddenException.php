<?php
namespace Jcurny\Sdk\Exception\Business;

class ForbiddenException extends AbstractException
{
    protected $message = 'Forbidden exception';
    protected $code = 403;
}
