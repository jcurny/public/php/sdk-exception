<?php
namespace Jcurny\Sdk\Exception\Technical\Runtime\ELException;

class CloneNotSupportedException extends \Jcurny\Sdk\Exception\Technical\Runtime\AbstractRuntimeException
{
    protected $message = 'Clone not supported exception';
}
