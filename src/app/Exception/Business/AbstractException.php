<?php
namespace Jcurny\Sdk\Exception\Business;

abstract class AbstractException extends \Jcurny\Sdk\Exception\AbstractException
{
    protected $message = 'Business exception';
    protected $code = 400;
}
