<?php
namespace Jcurny\Sdk\Exception\Technical\Runtime;

class IllegalArgumentException extends AbstractRuntimeException
{
    protected $message = 'Illegal argument exception';
}
