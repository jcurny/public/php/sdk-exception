<?php
namespace Jcurny\Sdk\Exception\Business\Media;

class MediaTooBigException extends \Jcurny\Sdk\Exception\Business\BadRequestException
{
    protected $message = 'Media too big exception';
}
